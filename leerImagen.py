import cv2 as cv

#leer imagen con librería openCV
#imagen = cv.imread('./imagenes/lena.png')

#convierte a escala de grises
#imagen_gray = cv.imread('./imagenes/lena.png', cv.IMREAD_GRAYSCALE)

# 1 pto acceder dentro de la carpeta actual
# 2 ptos acceder fuera de la carpeta actual
imagen_gray = cv.imread('../Externo/lapiz.jpg', cv.IMREAD_GRAYSCALE)

# Establecimiento de niveles de umbrales
#(thresh, im_bw) = cv.threshold(imagen_gray, 10, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
# otro método
(thresh, im_bw) = cv.threshold(imagen_gray, 175, 255, cv.THRESH_BINARY)

# a binario NO
#img_gris = cv.cvtColor(imagen, cv.COLOR_BGR2GRAY)

#muestra pantalla con título
cv.imshow('Lenna',imagen_gray)
cv.imshow('Lenna1',im_bw)

#guardar
cv.imwrite('./Salida/gris.png', imagen_gray)
cv.imwrite('./Salida/bw.png', im_bw)

#espera para que la ventana se destrya
cv.waitKey(0)

#espera al presionar una tecla para cerrar
cv.destroyAllWindows()