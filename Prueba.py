# -*- coding: utf-8 -*-
"""
Created on Mon Aug 17 00:13:55 2020

@author: marce
"""

import numpy as np
import math
import pygame
#import pygame.math as math
from pygame.locals import*

from OpenGL.GL import *
from OpenGL.GLU import *

points = [( 1, -1, -1),
          ( 1,  1, -1),
          (-1,  1, -1),
          (-1, -1, -1),
          ( 1, -1,  1),
          ( 1,  1,  1),
          (-1, -1,  1),
          (-1,  1,  1)
          ]

edges = [(0, 1),
         (0, 3),
         (0, 4),
         (2, 1),
         (2, 3),
         (2, 7),
         (6, 3),
         (6, 4),
         (6, 7),
         (5, 1),
         (5, 4),
         (5, 7)
         ]

tri = [( 1, -1, -2),
       ( 1,  1, -1),
       (-1,  1, -1)
       ]


def Cube():
    # glPointSize(5)
    # glBegin(GL_POINTS)
    glBegin(GL_LINES)
    # glBegin(GL_LINE_STRIP)
    #glBegin(GL_LINE_LOOP)
    for edge in edges:
        for vertex in edge:
            glVertex3fv(points[vertex])
    glEnd()


def Circle():
    # glPointSize(5)
    # glBegin(GL_POINTS)
    glBegin(GL_LINES)
    # glBegin(GL_LINE_STRIP)
    #glBegin(GL_LINE_LOOP)
    for i in np.arange(0.0, 10.0, 0.1):
        x = 2 * math.cos(i)
        y = 2 * math.sin(i)
        glVertex3f(x, y, 0)
    glEnd()


def Triangle():
    glColor3f(1, 0, 0)
    glBegin(GL_TRIANGLES)
    # glPointSize(10)
    # (GL_POINTS)
    # glBegin(GL_LINE_STRIP)
    # glBegin(GL_LINE_LOOP)
    glVertex3fv(tri[0])
    glVertex3fv(tri[1])
    glVertex3fv(tri[2])
    glEnd()


def Espiral():
    PI = 3.1415
    # glPointSize(5)
    # glBegin(GL_POINTS)
    # glLineWidth(3)
    # glBegin(GL_LINES)
    glBegin(GL_LINE_STRIP)
    # glBegin(GL_LINE_LOOP)

    z = -10.0
    for i in np.arange(0.0, (2.0 * PI) * 3.0, 0.1):
        x = 5 * math.sin(i)
        y = 5 * math.cos(i)
        glVertex3f(x, y, z)
        z += 0.1
    glEnd()


def stipple():
    glEnable(GL_LINE_STIPPLE)
    glLineStipple(2, 1)
    # glBegin(GL_LINES)
    # glBegin(GL_LINE_STRIP)
    glBegin(GL_LINES)
    for i in np.arange(0.0, 10.0, 0.1):
        x = 2 * math.cos(i)
        y = 2 * math.sin(i)
        glVertex3f(x, y, 0)
    glEnd()


def main():
    # Inicializa la pantalla
    pygame.init()

    # Define caracteristicas de la pantall
    display = (800, 600)
    pygame.display.set_mode(display, DOUBLEBUF | OPENGL)

    # Especifica el tamaño de la ventana
    glViewport(0, 0, 800, 600)

    # Define el color del fondo/default negro
    glClearColor(0, 0, 0, 0)

    # Matriz de transformaciones
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    # gluPerspective(field_view(degrees), aspect ratio, near-far clipping plane,  )
    gluPerspective(45, (display[0] / display[1]), 0.1, 200.0)

    # Matriz de Proyeccion 3D a 2D
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glTranslatef(0.0, 0.0, -5)
    # glScalef(0.8, 0.8, 0.8)
    glRotatef(45, 0, 1, 0)

    # Display
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        glRotatef(0.1, 2, 1, 1)

        # Limpia la pantalla
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Funciones
        #Cube()
        Circle()
        #Triangle()
        #Espiral()
        # stipple()

        # actualizamos la pantalla
        pygame.display.flip()
        # Define TIempo
        pygame.time.wait(10)
main()


