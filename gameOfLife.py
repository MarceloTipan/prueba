# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 18:29:45 2020

@author: marce
"""

import pygame
import numpy as np
import time

# crear pantalla de juego
pygame.init()

# ancho y alto de pantalla
width, height = 1000, 1000

# creacion de la pantalla
screen = pygame.display.set_mode((height, width))

# color del fondo = casi negro, casi oscuro
bg = 25, 25, 25

# pintamos en fondo con el color anterior
screen.fill(bg)

# cantidad de celdas en x e y
nxC, nyC = 100, 100

# ancho de la pantalla
dimCW = width / nxC
dimCH = height / nyC

# donde se guarda la informacion de los cuadrados del juego
# estado de las celdas vivas=1 muertas=0
gameState = np.zeros((nxC, nyC))

# ejemplo, automata
gameState[2, 2] = 1
gameState[2, 3] = 1
gameState[2, 4] = 1

gameState[20, 10] = 1
gameState[20, 11] = 1
gameState[20, 12] = 1

gameState[15, 15] = 1
gameState[15, 17] = 1
gameState[16, 16] = 1
gameState[16, 17] = 1
gameState[17, 16] = 1

gameState[5, 15] = 1
gameState[5, 17] = 1
gameState[6, 16] = 1
gameState[6, 17] = 1
gameState[7, 16] = 1

gameState[11, 5] = 1
gameState[11, 7] = 1
gameState[12, 6] = 1
gameState[12, 7] = 1

# control de la ejecución del juego
pauseExect = False

# blucle de ejecución
while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    # respaldo
    newGameState = np.copy(gameState)

    # volver a pintar todo, para no superponer las casillas ya pintadas
    screen.fill(bg)

    # cada fotograma da un respiro
    time.sleep(0.1)

    # registrar evento de teclado
    ev = pygame.event.get()

    for event in ev:

        # Detecta si se presiona una tecla
        if event.type == pygame.KEYDOWN:
            pauseExect = not pauseExect

        # Detectanis si se pulsa el ratón
        mouseCLick = pygame.mouse.get_pressed()

        if sum(mouseCLick) > 0:
            posX, posY = pygame.mouse.get_pos()
            celX, celY = int(np.floor(posX / dimCW)), int(np.floor(posY / dimCH))
            newGameState[celX, celY] = not mouseCLick[2]

    for y in range(0, nxC):
        for x in range(0, nyC):

            if not pauseExect:
                # calculamos el numero de vecinos cercanos
                n__neigh = gameState[(x - 1) % nxC, (y - 1) % nyC] + \
                           gameState[(x) % nxC, (y - 1) % nyC] + \
                           gameState[(x + 1) % nxC, (y - 1) % nyC] + \
                           gameState[(x - 1) % nxC, (y) % nyC] + \
                           gameState[(x + 1) % nxC, (y) % nyC] + \
                           gameState[(x - 1) % nxC, (y + 1) % nyC] + \
                           gameState[(x) % nxC, (y + 1) % nyC] + \
                           gameState[(x + 1) % nxC, (y + 1) % nyC]

                # print("x:",(x-1)% nxC, "y:",(y-1)% nxC)
                # print("x-1:",gameState[(x-1) % nxC ,(y-1) % nyC])
                # regla 1 : Una célula muerta con 3 vecinas vivas, revive
                if gameState[x, y] == 0 and n__neigh == 3:
                    newGameState[x, y] = 1

                # regla 2 : Una célula viva con menos de 2 o más de 3 vecinos vivos
                #          muere por soledad o muere por sobrepoblación
                elif gameState[x, y] == 1 and (n__neigh < 2 or n__neigh > 3):
                    newGameState[x, y] = 0

            # define el cuadrado que va a dibujar
            poly = [((x) * dimCW, y * dimCW),
                    ((x + 1) * dimCW, y * dimCH),
                    ((x + 1) * dimCW, (y + 1) * dimCH),
                    ((x) * dimCW, (y + 1) * dimCH)]

            # lugar donde se dibuja, color, que dibuja, (relleno) ancho de linea de dibujo
            if newGameState[x, y] == 0:
                pygame.draw.polygon(screen, (128, 128, 128), poly, 1)
            else:
                pygame.draw.polygon(screen, (255, 255, 255), poly, 0)

    # actualización de estado
    gameState = np.copy(newGameState)

    # actualizamos la pantalla
    pygame.display.flip()
